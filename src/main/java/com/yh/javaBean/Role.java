package com.yh.javaBean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Value("1")
    private int id;
    @Value("爱拼才会赢")
    private String name;
    @Value("冲冲冲！！！")
    private String desc;
}
