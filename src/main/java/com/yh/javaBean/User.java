package com.yh.javaBean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("u1")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Value("admin")
    private String username;
    @Value("admin")
    private String password;
    @Autowired
    private Role role;

}
